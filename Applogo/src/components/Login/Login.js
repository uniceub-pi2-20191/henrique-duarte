import React, {Component} from 'react';
import { StyleSheet, View, Image, Text ,TextInput } from 'react-native';
import Loginform from './Loginform';

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {text: ''};
      }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.logoContainer}>
            <Image
            style={styles.logo} 
            source={require('../../components/images/aleatory.png')} 
            />

            <Text style={styles.title}>Welcome to Aleatory Store App</Text>
            <View>
        <TextInput
          style={{height: 40}}
          placeholder="DON'T TYPE HERE!"
          onChangeText={(text) => this.setState({text})}
        />
        <Text style={{ fontSize: 32}}>
          {this.state.text.split(' ').map((word) => word && 'ME LIVREI DO SR').join(' ')}
        </Text>
      </View>
        </View>
        <View style={styles.formContainer}>
            <Loginform />
        </View>
      </View>
    );
  }
}
 
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#3498db'
    },
    logoContainer: {
        alignItems: 'center',
        flexGrow: 1,
        justifyContent: 'center'
    },
    logo: {
        width: 100,
        height: 100
    },
    title: {
        color: '#FFF',
        marginTop: 10,
        width: 160,
        textAlign: 'center',
        opacity: 0.9
    },
});
