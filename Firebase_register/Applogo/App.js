import React from 'react';

import { ActivityIndicator, StyleSheet, Text, View, TextInput } from 'react-native';

import * as firebase from 'firebase';

import { Input } from './components/Input';

import { Button } from './components/Button';



export default class App extends React.Component {

  state = {

    email: '',

    password: '',

    authenticating: false,

    user: null,

    error: '',

  }



  componentWillMount() {

    const firebaseConfig = {

      apiKey: 'AIzaSyA9l9YqFdpixiZV3jMsPpHHahGdtkYwrqs',

      authDomain: 'pelagic-bastion-232419.firebaseapp.com',

    }



    firebase.initializeApp(firebaseConfig);

  }



  onPressSignIn() {

    this.setState({

      authenticating: true,

    });



    const { email, password } = this.state;



    firebase.auth().signInWithEmailAndPassword(email, password)

      .then(user => this.setState({

        authenticating: false,

        user,

        error: '',

      }))

      .catch(() => {

        // Login was not successful

        firebase.auth().createUserWithEmailAndPassword(email, password)

          .then(user => this.setState({

            authenticating: false,

            user,

            error: '',

          }))

          .catch(() => this.setState({

            authenticating: false,

            user: null,

            error: 'Erro ao registrar',

          }))

      })

  }



  onPressLogOut() {

    firebase.auth().signOut()

      .then(() => {

        this.setState({

          email: '',

          password: '',

          authenticating: false,

          user: null,

        })

      }, error => {

        console.error('Sign Out Error', error);

      });

  }



  renderCurrentState() {

    if (this.state.authenticating) {

      return (

        <View style={styles.form}>

          <ActivityIndicator size='large' />

        </View>

      )

    }



    if (this.state.user !== null) {

      return (

        <View style={styles.form}>

          <Text style={styles.texto}>Registrado sucesso</Text>

          <Button onPress={() => this.onPressLogOut()}>Sair</Button>

        </View>

      )

    }



    return (

      <View style={styles.form}>

        <TextInput

          placeholder='Digite seu email...'

          onSubmitEditing={() => this.passwordInput.focus()}

          keyboardType="email-address"

          autoCapitalize="none"
                
          autoCorrect={false}

          onChangeText={email => this.setState({ email })}

          value={this.state.email}

        />

        <TextInput

          placeholder='Digite sua senha...'

          label='Senha'

          secureTextEntry

          onChangeText={password => this.setState({ password })}

          value={this.state.password}

          ref={(input) => this.passwordInput = input}

        />

        <Button onPress={() => this.onPressSignIn()}>Registrar</Button>

        <Text>{this.state.error}</Text>

      </View>

    )



  }



  render() {

    return (

      <View style={styles.container}>

        {this.renderCurrentState()}

      </View>

    );

  }

}



const styles = StyleSheet.create({

  container: {

    flex: 1,

    padding: 20,

    alignItems: 'center',

    justifyContent: 'center',

    flexDirection: 'row',
    
    

  },

  form: {

    flex: 1,

    backgroundColor: 'rgba(255,255,255,0.3)',

    color: '#FFF',

  },
  texto: {
    fontSize: 20
  }

});